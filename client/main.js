import { Meteor } from 'meteor/meteor';
import { Template } from 'meteor/templating';
import { Notes } from '../imports/api/notes.js';
import '../imports/startup/accounts_config.js';


import './main.html';

Template.body.helpers({
  notes(){
    return Notes.find({}, { sort: { createdAt: -1 } });
  }
});

Template.add.events({
  'submit .add-form': function(event){
    event.preventDefault();

    // Get input value
    const target = event.target;
    const text = target.text.value;
    console.log(text);

    // Insert note into collection
    Meteor.call('notes.insert', text);

    // Clear form
    target.text.value = '';

    // Close modal
    $('#addModal').modal('close');

    return false;
  }
});

Template.note.events({
  'click .delete-note':function(){
    Meteor.call('notes.remove', this);
    return false;
  }
});
